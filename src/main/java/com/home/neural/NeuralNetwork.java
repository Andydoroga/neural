package com.home.neural;

import com.home.Network;
import org.jblas.DoubleMatrix;

import static com.home.util.MathUtil.*;

public class NeuralNetwork implements Network {

    private static final double MIN = 0.01d;

    private final double learningRate;

    private DoubleMatrix weightsHidden_1;
    private DoubleMatrix weightsHidden_2;
    private DoubleMatrix outputWeights;

    public NeuralNetwork(int inputSize, int hiddenSize, int hiddenSize2, int outputSize, double learningRate) {
        this.learningRate = learningRate;

        this.weightsHidden_1 = getRandom(hiddenSize, inputSize, MIN, 0.5d);
        this.weightsHidden_2 = getRandom(hiddenSize2, hiddenSize, MIN, 0.5d);
        this.outputWeights = getRandom(outputSize, hiddenSize2, MIN, 0.5d);
    }

    @Override
    public DoubleMatrix query(DoubleMatrix input) {
        DoubleMatrix hiddenInput = weightsHidden_1.mmul(input);
        DoubleMatrix hiddenOutput = sigmoid(hiddenInput);
        DoubleMatrix hiddenInput2 = weightsHidden_2.mmul(hiddenOutput);
        DoubleMatrix hiddenOutput2 = sigmoid(hiddenInput2);
        DoubleMatrix finalInput = outputWeights.mmul(hiddenOutput2);

        return sigmoid(finalInput);
    }

    @Override
    public void train(DoubleMatrix input, DoubleMatrix target) {
        DoubleMatrix hiddenInput1 = weightsHidden_1.mmul(input);
        DoubleMatrix hiddenOutput1 = sigmoid(hiddenInput1);

        DoubleMatrix hiddenInput2 = weightsHidden_2.mmul(hiddenOutput1);
        DoubleMatrix hiddenOutput2 = sigmoid(hiddenInput2);

        DoubleMatrix finalInput = outputWeights.mmul(hiddenOutput2);
        DoubleMatrix finalOutput = sigmoid(finalInput);

        DoubleMatrix outputErrors = target.sub(finalOutput);
        DoubleMatrix hiddenErrors2 = (outputWeights.transpose()).mmul(outputErrors);
        DoubleMatrix hiddenErrors1 = (weightsHidden_2.transpose()).mmul(hiddenErrors2);

        DoubleMatrix deltaOut = outputErrors.mul(finalOutput).mul((getOnes(finalOutput).sub(finalOutput)));
        DoubleMatrix outputWeightsDelta = deltaOut.mmul(hiddenOutput2.transpose()).mul(learningRate);

        DoubleMatrix deltaHidden2 = hiddenErrors2.mul(hiddenOutput2).mul((getOnes(hiddenOutput2).sub(hiddenOutput2)));
        DoubleMatrix deltaWeightsHidden2 = deltaHidden2.mmul(hiddenOutput1.transpose()).mul(learningRate);

        DoubleMatrix deltaHidden1 = hiddenErrors1.mul(hiddenOutput1).mul((getOnes(hiddenOutput1).sub(hiddenOutput1)));
        DoubleMatrix deltaWeightsHidden1 = deltaHidden1.mmul(input.transpose()).mul(learningRate);

        weightsHidden_1.addi(deltaWeightsHidden1);
        weightsHidden_2.addi(deltaWeightsHidden2);
        outputWeights.addi(outputWeightsDelta);
    }

    @Override
    public DoubleMatrix getInputHiddenW() {
        return weightsHidden_1;
    }

    @Override
    public DoubleMatrix getHiddenOutputW() {
        return outputWeights;
    }

}
