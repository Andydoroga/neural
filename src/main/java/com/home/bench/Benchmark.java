package com.home.bench;

import com.home.util.Training;

import java.util.List;

public final class Benchmark {

    public static void bench(Command command, List<Training> trainingSet) {
        long current = System.currentTimeMillis();
        command.execute(trainingSet);
        System.out.println("time: " + (System.currentTimeMillis() - current));
    }

}
