package com.home.bench;

import com.home.util.Training;

import java.util.List;

public interface Command {

    void execute(List<Training> trainingSet);

}
