package com.home;

import com.home.bench.Benchmark;
import com.home.bench.Command;
import com.home.neural.NeuralNetwork;
import org.jblas.DoubleMatrix;
import com.home.util.Training;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test implements Command {

    public static void main(String... args) {
        Command command = new Test();
        Benchmark.bench(command, testTrainingSet());
    }

    @Override
    public void execute(List<Training> trainingSet) {
        Network neuralNetwork = new NeuralNetwork(784, 100, 50, 10, 0.05);
        double max = 0;
        for (int i = 1; i < 111; i++) {
            System.out.print("Epoch: " + i + "  result: ");
            //int lines = 0;
            try (BufferedReader br = new BufferedReader(new FileReader(getFile("mnist_train.csv")))) {
                for (String line; (line = br.readLine()) != null; ) {
                    neuralNetwork.train(getInput(line), getTargets(line));
//                    lines++;
//                    if (lines % 500 == 0) {
//                        System.out.println("Lines: " + lines);
//                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            double res = check(neuralNetwork);
            max = max < res ? res : max;
        }

        System.out.println("Max: " + max);
    }

    private double check(Network neuralNetwork) {
        double successCount = 0.0;
        double allCount = 0.0;
        try (BufferedReader br = new BufferedReader(new FileReader(getFile("mnist_test.csv")))) {
            for (String line; (line = br.readLine()) != null; ) {
                int target = Integer.valueOf(line.substring(0, 1));
                DoubleMatrix res = neuralNetwork.query(getInput(line));

                int indexMax = 0;
                for (int i = 0; i < res.length; i++) {
                    indexMax = res.get(i) > res.get(indexMax) ? i : indexMax;
                }
                if (target == indexMax) {
                    successCount++;
                }
                allCount++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print(String.format("%.2f", successCount / allCount * 100.0));
        System.out.println();
        return successCount / allCount * 100.0;
    }

    private DoubleMatrix getTargets(String line) {
        int target = Integer.valueOf(line.substring(0, 1));
        DoubleMatrix targets = new DoubleMatrix(10).fill(0.01d);
        targets.fill(0.01);
        targets.put(target, 0.99);
        return targets;
    }

    private DoubleMatrix getInput(String line) {
        double[] numbers = Arrays.stream(line.substring(2).split(",")).mapToDouble(Double::valueOf).map(val -> val / 255.0 * 0.01 + 0.01).toArray();
        return new DoubleMatrix(numbers.length, 1, numbers);
    }

    private File getFile(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }

    private static List<Training> testTrainingSet() {
        DoubleMatrix ideal0 = new DoubleMatrix(new double[]{0.01});
        DoubleMatrix ideal1 = new DoubleMatrix(new double[]{0.99});

        DoubleMatrix input1 = new DoubleMatrix(2, 1, 0.99, 0.01);
        DoubleMatrix input2 = new DoubleMatrix(2, 1, 0.01, 0.01);
        DoubleMatrix input3 = new DoubleMatrix(2, 1, 0.01, 0.99);
        DoubleMatrix input4 = new DoubleMatrix(2, 1, 0.99, 0.99);

        List<Training> trainingSet = new ArrayList<>();

        trainingSet.add(new Training(input1, ideal1));
        trainingSet.add(new Training(input2, ideal0));
        trainingSet.add(new Training(input3, ideal1));
        trainingSet.add(new Training(input4, ideal0));

        return trainingSet;
    }

}
