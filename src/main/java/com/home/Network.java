package com.home;

import org.jblas.DoubleMatrix;

public interface Network {
    DoubleMatrix query(DoubleMatrix input);

    void train(DoubleMatrix input, DoubleMatrix target);

    DoubleMatrix getInputHiddenW();

    DoubleMatrix getHiddenOutputW();
}
