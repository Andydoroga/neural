package com.home.util;

import lombok.Data;
import org.jblas.DoubleMatrix;

@Data
public class Training {
    private final DoubleMatrix input;
    private final DoubleMatrix target;
    private DoubleMatrix result;
}